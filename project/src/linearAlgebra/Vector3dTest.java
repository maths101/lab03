/* Grigor Mihaylov 1937997 */
package linearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTest {

	@Test
	public void testConstructAndGet() {
		Vector3d v = new Vector3d(4.0,6.5,9.78);
		
		assertEquals(4.0, v.getX());

		assertEquals(6.5, v.getY());

		assertEquals(9.78, v.getZ());
		
	}
	
	@Test
	public void testMagnitude() {
		double x=-10.11;
		double y=6.5;
		double z= 9.78;
		Vector3d v = new Vector3d(-10.11,6.5,9.78);
		assertEquals( Math.sqrt(x*x + y*y + z*z), v.magnitude());

	}
	
	@Test
	public void testDotProduct() {
		Vector3d v1 = new Vector3d(4.0,6.5,9.78);
		Vector3d v2 = new Vector3d(-10.11,6.5,9.78);
		double expected = 4.0 * -10.11 + 6.5* 6.5 + 9.78*9.78;
		assertEquals(expected,v1.dotProduct(v2));
	}
	
	@Test 
	public void testAdd() {
		Vector3d v1 = new Vector3d(4.0,6.5,9.78);
		Vector3d v2 = new Vector3d(-10.11,6.5,9.78);
		Vector3d v3 = v1.add(v2);
		
		assertEquals(-10.11 + 4.0, v3.getX());
		
		assertEquals(6.5+6.5, v3.getY());
		
		assertEquals(9.78 + 9.78, v3.getZ());
		
	}

}
