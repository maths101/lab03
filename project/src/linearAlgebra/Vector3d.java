/*Grigor Mihaylov 1937997 */

package linearAlgebra;

public class Vector3d {
	private double x;
	private double y;
	private double z;
	public double getX() {
		return this.x;
	}
	public double getY() {
		return this.y;
	}
	public double getZ() {
		return this.z;
	}
	public Vector3d(double x,double y,double z ){
		this.x=x;
		this.y=y;
		this.z=z;
	}
	public double magnitude() {
		return Math.sqrt(x*x + y*y + z*z);
	}
	public double dotProduct(Vector3d v) {
		double result ;
		result = this.x*v.getX()+ this.y*v.getY()+ this.z*v.getZ();
		return result;
	}
	public Vector3d add(Vector3d v) {
		double newX = this.x+v.getX();
		double newY  = this.y+v.getY();
		double newZ  = this.z+v.getZ();
		Vector3d result = new Vector3d(newX, newY,newZ);
		return result;
	}
}
